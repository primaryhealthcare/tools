import { knuthShuffle as shuffle } from 'knuth-shuffle';
import { jStat } from 'jstat';

import { round as mathRound } from 'mathjs';

export const round = (n, to = 4) => mathRound(n, to)

export const getrandom = ({
    universe,
    samplecontrol,
    sampletest,
    experiments
}) => [...Array(experiments).keys()].map(()=> {
    const control = shuffle([...universe.control]).slice(0, samplecontrol)
    const test = shuffle([...universe.test]).slice(0, sampletest)

    return {
        control: {
            good: countGood(control),
            bad: countBad(control)
        },
        test: {
            good: countGood(test),
            bad: countBad(test)
        }
    }
})

const countGood = (people) => people.filter(one => one.outcome === "good").length
const countBad = (people) => people.filter(one => one.outcome === "bad").length

const good = (n) => Array(n).fill({"outcome": "good"})
const bad = (n) => Array(n).fill({"outcome": "bad"})

export const generateuniverse = ({
    controlgood,
    controlbad,
    testgood,
    testbad
}) => ({
    control: [...good(controlgood), ...bad(controlbad)],
    test: [...good(testgood), ...bad(testbad)]
})

const OminusEWholeSquareByE = (o, e) => Math.pow(o - e, 2) / e
const expected = (r, c, total) => (r * c) / total
const chi2component = (a, r, c, total) => OminusEWholeSquareByE(a, expected(r, c, total));
const AbsOfOminusEMinusPoint5SquareByE = (o, e) => Math.pow(Math.abs(o - e) - 0.5, 2) / e
const chi2componentWithYates = (a, r, c, total) => AbsOfOminusEMinusPoint5SquareByE(a, expected(r, c, total));

export const getchisquare = (result, {yatesCorrection = false} = {}) => {
    const a = result.control.good
    const b = result.control.bad
    const c = result.test.good
    const d = result.test.bad
    const r1 = a + b
    const r2 = c + d
    const c1 = a + c
    const c2 = b + d

    const total = r1 + r2;

    const chi2function = yatesCorrection ? chi2componentWithYates : chi2component

    const chi2components = [
          chi2function(a, r1, c1, total),
          chi2function(b, r1, c2, total),
          chi2function(c, r2, c1, total),
          chi2function(d, r2, c2, total)
    ]
    return jStat.sum(chi2components)
}

export const getlefttailpvalue = (result, options) => jStat.chisquare.cdf(getchisquare(result, options), 1)

export const getrighttailpvalue = (result, options) => 1 - getlefttailpvalue(result, options)